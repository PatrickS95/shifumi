package jl.patou.shifumi.game;

import java.io.Serializable;

/**
 * Created by patrick on 1/18/18.
 */

public enum RoundScore implements Serializable {

    LOSE(0),
    TIE(0),
    WIN(1);

    private final int score;

    private RoundScore(final int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "RoundScore{ name=" + this.name() + "; score=" + score +
                '}';
    }

}
