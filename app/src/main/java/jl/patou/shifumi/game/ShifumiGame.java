package jl.patou.shifumi.game;

/**
 * Created by patrick on 1/22/18.
 */

public class ShifumiGame {

    private int score;

    // booleans to determine which have to play next;
    private boolean currentPlayerHavePlayed = false;
    private boolean opponentHavePlayed = false;

    public ShifumiGame() {
        this.score = 0;
        this.currentPlayerHavePlayed = false;
        this.opponentHavePlayed = false;
    }

    public void updateScore(RoundScore roundScore) {
        this.score = this.score + roundScore.getScore();
    }


    public boolean canThePlayerPlay() {
        if (!currentPlayerHavePlayed)
            return true;

        if (currentPlayerHavePlayed && opponentHavePlayed)
            return true;

        return false;

    }

    /**
     * Put both boolean to false: current played have played and opponent have played
     */
    public void resetRound() {
        this.currentPlayerHavePlayed = false;
        this.opponentHavePlayed = false;
    }


    ////////////////// SETTER /////////////////////////////////////////

    public void setCurrentPlayerHavePlayed(boolean currentPlayerHavePlayed) {
        this.currentPlayerHavePlayed = currentPlayerHavePlayed;
    }

    public void setOpponentHavePlayed(boolean opponentHavePlayed) {
        this.opponentHavePlayed = opponentHavePlayed;
    }

    ////////////////// GETTER /////////////////////////////////////////
    public int getScore() {
        return score;
    }

    public boolean haveCurrentPlayerPlayed() {
        return currentPlayerHavePlayed;
    }

    public boolean haveOpponentPlayed() {
        return opponentHavePlayed;
    }
}
