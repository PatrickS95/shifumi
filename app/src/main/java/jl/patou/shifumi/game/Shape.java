package jl.patou.shifumi.game;

import java.io.Serializable;

/**
 * Created by patrick on 1/18/18.
 */

public enum Shape implements Serializable {

    ROCK,
    PAPER,
    SCISSORS;

    static {
        ROCK.defeatedShape = SCISSORS;
        ROCK.name = "Rock";

        PAPER.defeatedShape = ROCK;
        PAPER.name = "Paper";

        SCISSORS.defeatedShape = PAPER;
        SCISSORS.name = "Scissors";
    }

    private Shape defeatedShape;
    private String name;

    public Shape getDefeatedShape() {
        return this.defeatedShape;
    }

    public String getName() {
        return name;
    }

    public RoundScore againstAnotherShape(Shape shape) {


        if (this.equals(shape))
            return RoundScore.TIE;

        else if (this.defeatedShape.equals(shape))
            return RoundScore.WIN;

        else
            return RoundScore.LOSE;
    }

    @Override
    public String toString() {
        return "Shape : " + this.name + " defeats: " + defeatedShape.name;
    }
}


