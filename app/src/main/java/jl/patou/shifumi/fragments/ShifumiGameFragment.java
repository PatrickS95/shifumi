package jl.patou.shifumi.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import jl.patou.shifumi.R;
import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.game.Shape;
import jl.patou.shifumi.onclicklistener.ShapeOnClickListener;

/**
 * Created by patrick on 1/26/18.
 */

public class ShifumiGameFragment extends Fragment {

    private ImageButton rock;
    private ImageButton paper;
    private ImageButton scissors;

    private TextView score;
    private TextView roundInfo;
    private TextView lastSendShape;
    private TextView lastReceivedShape;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_shifumi_game, container, false);


        this.rock = view.findViewById(R.id.rockImageButton);
        this.paper = view.findViewById(R.id.paperImageButton);
        this.scissors = view.findViewById(R.id.scissorsImageButton);

        view.findViewById(R.id.rockImageButton).setOnClickListener(new ShapeOnClickListener((ShifumiActivity) getActivity(), Shape.ROCK));
        view.findViewById(R.id.paperImageButton).setOnClickListener(new ShapeOnClickListener((ShifumiActivity) getActivity(), Shape.PAPER));
        view.findViewById(R.id.scissorsImageButton).setOnClickListener(new ShapeOnClickListener((ShifumiActivity) getActivity(), Shape.SCISSORS));


        this.score = view.findViewById(R.id.score);
        this.roundInfo = view.findViewById(R.id.roundInfo);
        this.lastSendShape = view.findViewById(R.id.userShape);
        this.lastReceivedShape = view.findViewById(R.id.opponentShape);

        return view;
    }

    /**
     * Enables or disables the sendShape button;
     *
     * @param enabled true = enable ; false = disable
     */
    public void setShapeButtonsEnabled(Boolean enabled) {

        this.rock.setClickable(enabled);
        this.rock.setEnabled(enabled);

        this.paper.setClickable(enabled);
        this.paper.setEnabled(enabled);

        this.scissors.setClickable(enabled);
        this.scissors.setEnabled(enabled);
    }

    public void setScoreInTextView(int score) {
        this.score.setText(getActivity().getResources().getString(R.string.score) + " " + score);
    }

    public void setTextRoundInfo(String roundInfo) {
        this.roundInfo.setText(roundInfo);
    }

    public void setLastSendShape(Shape sendShape) {
        this.lastSendShape.setText(getActivity().getResources().getString(R.string.playerShape) + " " + sendShape.getName().toLowerCase());
    }

    public void setLastReceivedShape(Shape receivedShape) {
        this.lastReceivedShape.setText(getActivity().getResources().getString(R.string.opponentShape) + " " + receivedShape.getName().toLowerCase());
    }

    public void setRoundInfoVisible(Boolean visible) {
        if (visible)
            this.roundInfo.setVisibility(View.VISIBLE);
        else
            this.roundInfo.setVisibility(View.INVISIBLE);
    }

}
