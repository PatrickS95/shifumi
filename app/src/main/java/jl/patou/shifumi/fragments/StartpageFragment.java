package jl.patou.shifumi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import jl.patou.shifumi.R;
import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.adapters.W2pDevicesAdapter;
import jl.patou.shifumi.onclicklistener.OnDeviceItemClickListener;

/**
 * Created by patrick on 1/26/18.
 */

public class StartpageFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_startpage, container, false);

        ShifumiActivity shifumiActivity = (ShifumiActivity) getActivity();


        ListView deviceListView = (ListView) view.findViewById(R.id.w2pDeviceList);
        deviceListView.setOnItemClickListener(new OnDeviceItemClickListener(shifumiActivity));

        W2pDevicesAdapter w2pDevicesAdapter = new W2pDevicesAdapter(shifumiActivity, R.layout.activity_shifumi, shifumiActivity.getPeers());
        deviceListView.setAdapter(w2pDevicesAdapter);


        shifumiActivity.setW2pDevicesListView(deviceListView);
        shifumiActivity.setW2pDevicesAdapter(w2pDevicesAdapter);

        return view;
    }

}
