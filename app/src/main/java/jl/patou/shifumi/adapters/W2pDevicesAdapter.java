package jl.patou.shifumi.adapters;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import jl.patou.shifumi.R;

/**
 * Created by patrick on 1/25/18.
 */

public class W2pDevicesAdapter extends ArrayAdapter<WifiP2pDevice> {
    public W2pDevicesAdapter(@NonNull Context context, int resource, @NonNull List<WifiP2pDevice> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        WifiP2pDevice wifiP2pDevice = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.wifi_device, parent, false);
        }
        // Lookup view for data population
        TextView deviceName = (TextView) convertView.findViewById(R.id.deviceName);

        // Populate the data into the template view using the data object
        deviceName.setText(wifiP2pDevice.deviceName);

        // Return the completed view to render on screen
        return convertView;
    }
}
