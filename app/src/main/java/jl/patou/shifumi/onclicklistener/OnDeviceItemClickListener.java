package jl.patou.shifumi.onclicklistener;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.View;
import android.widget.AdapterView;

import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.p2p.actionlisteners.P2pGroupCreationActionListener;

/**
 * Created by patrick on 1/25/18.
 */

public class OnDeviceItemClickListener implements AdapterView.OnItemClickListener {

    private final ShifumiActivity shifumiActivity;
    private WifiP2pDevice deviceToConnect;

    public OnDeviceItemClickListener(ShifumiActivity shifumiActivity) {
        this.shifumiActivity = shifumiActivity;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        this.deviceToConnect = this.shifumiActivity.getPeers().get(i);

        new AlertDialog.Builder(this.shifumiActivity)
                .setTitle("Connect with device")
                .setMessage("Do you really want to connect with " + this.deviceToConnect.deviceName + " ?")

                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        connect(deviceToConnect);
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }


    private void connect(WifiP2pDevice deviceToConnect) {

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = deviceToConnect.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        this.shifumiActivity.getManagerP2p().connect(this.shifumiActivity.getChannelP2p(), config, new P2pGroupCreationActionListener(this.shifumiActivity));
    }
}
