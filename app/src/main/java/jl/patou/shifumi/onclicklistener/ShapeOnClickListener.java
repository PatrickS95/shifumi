package jl.patou.shifumi.onclicklistener;

import android.view.View;

import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.game.Shape;

/**
 * Created by patrick on 1/22/18.
 */

public class ShapeOnClickListener implements View.OnClickListener {

    private ShifumiActivity shifumiActivity;
    private Shape shape;

    public ShapeOnClickListener(ShifumiActivity shifumiActivity, Shape shape) {
        this.shape = shape;
        this.shifumiActivity = shifumiActivity;
    }

    @Override
    public void onClick(View view) {
        this.shifumiActivity.sendShape(shape);
    }
}
