package jl.patou.shifumi.p2p.actionlisteners;

import android.net.wifi.p2p.WifiP2pManager;

import jl.patou.shifumi.ShifumiActivity;

/**
 * Created by patrick on 1/18/18.
 */

public class P2pGroupCreationActionListener implements WifiP2pManager.ActionListener {


    private ShifumiActivity shifumiActivity;

    public P2pGroupCreationActionListener(ShifumiActivity shifumiActivity) {
        this.shifumiActivity = shifumiActivity;
    }

    @Override
    public void onSuccess() {

        if (this.shifumiActivity.isConnectionIsActive())
            this.shifumiActivity.logAndToast("A WIFI-Direct connection is already active, you can't connect a second time");
        else
            this.shifumiActivity.logAndToast("Asking for connection");
    }

    @Override
    public void onFailure(int reason) {
        if (reason == WifiP2pManager.P2P_UNSUPPORTED)
            this.shifumiActivity.logAndToast("P2p is unsupported by the device");
        else if (reason == WifiP2pManager.ERROR)
            this.shifumiActivity.logAndToast("For a unknow reason the the connection couldn't be established, please try agains");
        else if (reason == WifiP2pManager.BUSY)
            this.shifumiActivity.logAndToast("The device is busy, has it already a WIFI-Direct connection? ");
    }
}
