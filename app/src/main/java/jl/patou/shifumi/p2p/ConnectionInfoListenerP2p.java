package jl.patou.shifumi.p2p;

import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.socket.SocketConnectionRole;

/**
 * Created by patrick on 1/18/18.
 */

public class ConnectionInfoListenerP2p implements WifiP2pManager.ConnectionInfoListener {

    private ShifumiActivity shifumiActivity;


    public ConnectionInfoListenerP2p(ShifumiActivity shifumiActivity) {
        this.shifumiActivity = shifumiActivity;
    }


    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {

        InetAddress groupOwnerAddress = null;
        try {
            groupOwnerAddress = InetAddress.getByName(wifiP2pInfo.groupOwnerAddress.getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            Log.e(ShifumiActivity.TAG, "UnknownHostException!");
            return;
        }

        SocketConnectionRole socketConnectionRole;

        // Group negotiation is done now determining group owner
        if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {

            // this device is the groupowner or server
            socketConnectionRole = SocketConnectionRole.SERVER;

        } else if (wifiP2pInfo.groupFormed) {
            // this device is the client
            socketConnectionRole = SocketConnectionRole.CLIENT;
        } else { // should never come here
            this.shifumiActivity.logAndToast("Error while connecting please try again");
            return;
        }


        // update attributes in shifumiActivity

        this.shifumiActivity.setSocketRole(socketConnectionRole);
        this.shifumiActivity.setServerInetAddress(groupOwnerAddress);
        this.shifumiActivity.setConnectionIsActive(true);

        // make UI-changes
        this.shifumiActivity.activateP2pGroupInView(socketConnectionRole, groupOwnerAddress);

        // Wether it's the client nor the server they have to start the connection
        this.shifumiActivity.startSocketConnection();


    }


}
