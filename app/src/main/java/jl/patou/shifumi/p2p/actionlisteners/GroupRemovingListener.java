package jl.patou.shifumi.p2p.actionlisteners;

import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import jl.patou.shifumi.ShifumiActivity;

/**
 * Created by patrick on 1/19/18.
 */

public class GroupRemovingListener implements WifiP2pManager.ActionListener {
    @Override
    public void onSuccess() {
        Log.i(ShifumiActivity.TAG, "Group was removed!");
    }

    @Override
    public void onFailure(int i) {
        Log.i(ShifumiActivity.TAG, "Group couldn't be removed!");
    }
}
