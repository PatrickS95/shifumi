package jl.patou.shifumi.p2p.actionlisteners;

import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import jl.patou.shifumi.ShifumiActivity;

/**
 * Created by patrick on 1/18/18.
 */

public class DiscoverPeersActionListener implements WifiP2pManager.ActionListener {


    public DiscoverPeersActionListener() {
        super();
    }

    @Override
    public void onSuccess() {
        // Code for when the discovery initiation is successful goes here.
        // No services have actually been discovered yet, so this method
        // can often be left blank. Code for peer discovery goes in the
        // onReceive method, detailed below.
        Log.i(ShifumiActivity.TAG, "Peers can now be discovered");

    }

    @Override
    public void onFailure(int reasonCode) {
        // Code for when the discovery initiation fails goes here.
        // Alert the user that something went wrong.

        Log.i(ShifumiActivity.TAG, "onFailure()");
    }


}
