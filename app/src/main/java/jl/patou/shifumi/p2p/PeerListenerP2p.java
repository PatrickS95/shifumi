package jl.patou.shifumi.p2p;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;

import java.util.ArrayList;
import java.util.List;

import jl.patou.shifumi.ShifumiActivity;


/**
 * Created by patrick on 1/18/18.
 */

public class PeerListenerP2p implements WifiP2pManager.PeerListListener {

    private ShifumiActivity shifumiActivity;


    public PeerListenerP2p(ShifumiActivity shifumiActivity) {
        this.shifumiActivity = shifumiActivity;
    }


    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {

        List<WifiP2pDevice> refreshedPeers = new ArrayList<WifiP2pDevice>(peerList.getDeviceList());
        if (!refreshedPeers.equals(shifumiActivity.getPeers())) {

            if (refreshedPeers.size() == 0)
                this.shifumiActivity.clearPeers();

            else
                this.shifumiActivity.updatePeerList(refreshedPeers);

        }
    }


}
