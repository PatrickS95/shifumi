package jl.patou.shifumi.p2p.actionlisteners;

import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.widget.Toast;

import jl.patou.shifumi.ShifumiActivity;

/**
 * Created by patrick on 1/18/18.
 * <p>
 * <p>
 * This class/listener is currently not used, because Android handles
 * Group creation itself
 * Useful when you have devices that doesn't have WIFI-Direct
 */

public class CreateGroupActionListener implements WifiP2pManager.ActionListener {

    private ShifumiActivity shifumiActivity;


    public CreateGroupActionListener(ShifumiActivity shifumiActivity) {
        this.shifumiActivity = shifumiActivity;
    }

    @Override
    public void onSuccess() {
        // Device is ready to accept incoming connections from peers.

        Log.i(ShifumiActivity.TAG, "I'm the owner now of the group, I accept connections from other peers!");
    }

    @Override
    public void onFailure(int reason) {
        Toast.makeText(shifumiActivity, "P2P group creation failed. Retry.",
                Toast.LENGTH_LONG).show();
    }


}
