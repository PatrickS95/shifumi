package jl.patou.shifumi.p2p;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.os.IBinder;
import android.util.Log;

import jl.patou.shifumi.ShifumiActivity;

/**
 * Created by patrick on 1/18/18.
 * <p>
 * A BroadcastReceiver that notifies of important Wi-Fi p2p events.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager wifiManager;
    private Channel wifiChannel;
    private ShifumiActivity shifumiActivity;
    private ConnectionInfoListener connectionInfoListener; // determines roles and IP adresses in P2P group

    public WiFiDirectBroadcastReceiver(WifiP2pManager wifiManager, Channel wifiChannel, ShifumiActivity shifumiActivity) {
        super();

        this.wifiManager = wifiManager;
        this.wifiChannel = wifiChannel;
        this.shifumiActivity = shifumiActivity;

        this.connectionInfoListener = new ConnectionInfoListenerP2p(this.shifumiActivity);
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) { // tells you if P2P is activated


            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi P2P is enabled
                Log.i(ShifumiActivity.TAG, "Wifi P2P is enabled");

            } else {
                // Wi-Fi P2P is not enabled
                this.shifumiActivity.clearPeers();
                shifumiActivity.logAndToast("Wifi P2P is not enabled... You have to activate your WIFI");
                this.shifumiActivity.deactivateConnection(true);
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) { // peer list has changed.

            // Request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            if (shifumiActivity.getManagerP2p() != null) {
                shifumiActivity.getManagerP2p().requestPeers(shifumiActivity.getChannelP2p(), shifumiActivity.getPeerListenerP2p());

            } else {
                this.shifumiActivity.deactivateConnection(true);
            }


        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) { // state of Wi-Fi P2P connectivity has changed.


            if (shifumiActivity.getManagerP2p() == null) { // the P2P couldn't be intialized properly // that's why there is nothing to do
                this.shifumiActivity.deactivateConnection(false);

                return;
            }

            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) { //

                // We are connected with the other device, request connection
                // info to find group owner IP
                shifumiActivity.getManagerP2p().requestConnectionInfo(shifumiActivity.getChannelP2p(), this.connectionInfoListener);
                shifumiActivity.setConnectionIsActive(true);

            } else {
                this.shifumiActivity.deactivateConnection(true);
            }


        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) { // device's configuration details have changed.

            Log.i(ShifumiActivity.TAG, "Device's configuration details have changed"); // this means a lot..

        }
    }

}
