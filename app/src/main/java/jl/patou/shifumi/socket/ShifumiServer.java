package jl.patou.shifumi.socket;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.game.Shape;
import jl.patou.shifumi.socket.game.ShifumiGameHandler;

/**
 * Created by patrick on 1/18/18.
 */

public class ShifumiServer extends Thread implements ShifumiDevice {

    public final static int SERVER_PORT = 8888;
    private int serverPort;
    private ServerSocket serverSocket;
    private Socket socket;
    private ShifumiActivity shifumiActivity;
    private OutputStream out;
    private ObjectOutputStream objectOut;
    private ShifumiGameHandler shifumiGameHandler;


    public ShifumiServer(int serverPort, ShifumiActivity shifumiActivity) {
        this.serverPort = serverPort;
        this.shifumiActivity = shifumiActivity;
    }


    @Override
    public void run() {
        super.run();

        try {
            this.serverSocket = new ServerSocket();
            this.serverSocket.setReuseAddress(true);
            this.serverSocket.bind(new InetSocketAddress(serverPort));
            //this.serverSocket = new ServerSocket();
            Log.i(ShifumiActivity.TAG, "Listenning on port: " + serverPort);

            this.socket = this.serverSocket.accept();

            this.shifumiActivity.loadGameFragment();

            out = socket.getOutputStream();
            objectOut = new ObjectOutputStream(out);

            this.shifumiGameHandler = new ShifumiGameHandler(socket, shifumiActivity);
            this.shifumiGameHandler.startHandler();


        } catch (IOException e) {
            e.printStackTrace();
            Log.e(ShifumiActivity.TAG, "ServerSocket couldn't be launched because1: " + e.getMessage());
        }
    }

    public void sendShape(Shape shape) {
        this.shifumiGameHandler.sendShape(shape);
    }

    @Override
    public void stopSocketConnection() {
        try {
            Log.i(ShifumiActivity.TAG, "Stop Server socket connection");
            this.shifumiGameHandler.setHandlerStopped(true);
            objectOut.close();
            out.close();
            this.socket.close();
            this.serverSocket.close();
            this.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
