package jl.patou.shifumi.socket.game;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import jl.patou.shifumi.R;
import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.game.Shape;
import jl.patou.shifumi.game.ShifumiGame;

/**
 * Created by patrick on 1/19/18.
 */

/**
 * This handler runs within the client or server Thread.
 *
 * The handling of the game is the same if it's the client or the server
 */
public class ShifumiGameHandler {

    // necessary to get Input/output Streams of socket and server
    private Socket socket;
    private ShifumiActivity shifumiActivity;

    private InputStream in;
    private ObjectInputStream objectIn;

    private OutputStream out;
    private ObjectOutputStream objectOut;

    private Shape receivedShape;
    private Shape sendShape;

    private ShifumiGame shifumiGame;

    // volatile so java make sur that at every moment the value of the boolean is up to date
    private volatile boolean handlerStopped;

    public ShifumiGameHandler(Socket client, ShifumiActivity shifumiActivity) {
        this.socket = client;
        this.shifumiActivity = shifumiActivity;
        this.handlerStopped = false;
    }


    public void startHandler() {

        this.shifumiGame = new ShifumiGame();

        try {

            in = this.socket.getInputStream();
            objectIn = new ObjectInputStream(in);

            out = socket.getOutputStream();
            objectOut = new ObjectOutputStream(out);


            while (!this.handlerStopped) {

                try {
                    receivedShape = (Shape) objectIn.readObject();

                    if (this.shifumiGame.haveCurrentPlayerPlayed()) {

                        handleRoundIsOver();

                    } else {
                        // the current player can play

                        this.shifumiActivity.getmUiHandler().post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        shifumiActivity.logAndToast(shifumiActivity.getResources().getString(R.string.shapeWasReceived));
                                        shifumiActivity.setTextRoundInfo(shifumiActivity.getResources().getString(R.string.shapeWasReceived));
                                    }
                                }
                        );
                        this.shifumiGame.setOpponentHavePlayed(true);
                    }

                } catch (IOException e) {
                    // If it's not a shape it can be ignored
                }
            }

            // After the handler have been interrupted close everything
            objectIn.close();
            in.close();
            objectOut.close();
            out.close();
            this.socket.close();

        } catch (UnknownHostException e) {
            Log.i(ShifumiActivity.TAG, "UnknowHost: " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(ShifumiActivity.TAG, "IO Exception: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void sendShape(Shape shape) {

        if (this.shifumiGame.canThePlayerPlay()) {
            this.sendShape = shape;

            new SendShapeThread(shape, objectOut).start();
            this.shifumiGame.setCurrentPlayerHavePlayed(true);
            this.shifumiActivity.setShapeButtonsEnabled(false);

            if (this.shifumiGame.haveOpponentPlayed()) {

                // the "round" is over so update score and make UI updates in ShifumiActivity
                handleRoundIsOver();
            }
        }
    }


    /**
     * Handles state where a round is over: both player played in the round
     * Updates score + update in UI
     * reset round
     */
    public void handleRoundIsOver() {

        this.shifumiGame.updateScore(sendShape.againstAnotherShape(receivedShape));

        // the "round" is over so update score and make UI updates in ShifumiActivity
        this.shifumiActivity.getmUiHandler().post(
                new RoundOverRunnable(this.shifumiActivity, sendShape, receivedShape, shifumiGame.getScore())
        );

        this.shifumiGame.resetRound();
    }

    public void setHandlerStopped(boolean interrupt){
        this.handlerStopped = interrupt;
    }


}
