package jl.patou.shifumi.socket;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.game.Shape;
import jl.patou.shifumi.socket.game.ShifumiGameHandler;

/**
 * Created by patrick on 1/18/18.
 */

public class ShifumiClient extends Thread implements ShifumiDevice {


    private int serverPort;
    private InetAddress serverAdress;
    private Socket socket;
    private ShifumiActivity shifumiActivity;


    // communication objects, if not present then NullPointer
    private OutputStream out;
    private ObjectOutputStream objectOut;

    private ShifumiGameHandler shifumiGameHandler;


    public ShifumiClient(InetAddress serverAdress, int serverPort, ShifumiActivity shifumiActivity) {
        this.serverAdress = serverAdress;
        this.serverPort = serverPort;
        this.shifumiActivity = shifumiActivity;
    }

    @Override
    public void run() {
        super.run();


        /**
         * The socket inialization of the server side socket normaly takes more time
         * so try again until the socket-connection is initialized
         */
        while (this.socket == null) {
            try {
                Thread.sleep(200); // wait before trying
                this.socket = new Socket(this.serverAdress, this.serverPort);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.shifumiActivity.loadGameFragment();

        try {
            out = socket.getOutputStream();
            objectOut = new ObjectOutputStream(out);

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i(ShifumiActivity.TAG, "Client socket have been intialized");

        this.shifumiGameHandler = new ShifumiGameHandler(socket, shifumiActivity);
        this.shifumiGameHandler.startHandler();

    }

    @Override
    public void stopSocketConnection() {
        try {
            Log.i(ShifumiActivity.TAG, "Stop Client socket connection");
            this.socket.close();
            this.shifumiGameHandler.setHandlerStopped(true);
            objectOut.close();
            out.close();
            this.socket.close();
            this.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendShape(Shape shape) {
        this.shifumiGameHandler.sendShape(shape);
    }


}

