package jl.patou.shifumi.socket;

/**
 * Created by patrick on 1/18/18.
 */

public enum SocketConnectionRole {
    CLIENT,
    SERVER
}
