package jl.patou.shifumi.socket;

import jl.patou.shifumi.game.Shape;

/**
 * Created by patrick on 1/22/18.
 */

public interface ShifumiDevice {

    void sendShape(Shape shape);

    void stopSocketConnection();

    void start(); // because they are threads
}
