package jl.patou.shifumi.socket.game;

import jl.patou.shifumi.ShifumiActivity;
import jl.patou.shifumi.game.RoundScore;
import jl.patou.shifumi.game.Shape;

/**
 * Created by patrick on 1/24/18.
 */

public class RoundOverRunnable implements Runnable {

    private ShifumiActivity shifumiActivity;
    private Shape sendShape;
    private Shape receivedShape;
    private int score;

    public RoundOverRunnable(ShifumiActivity shifumiActivity, Shape sendShape, Shape receivedShape, int score) {
        this.shifumiActivity = shifumiActivity;
        this.sendShape = sendShape;
        this.receivedShape = receivedShape;
        this.score = score;
    }


    @Override
    public void run() {
        String roundMessage = "";
        RoundScore roundScore = sendShape.againstAnotherShape(receivedShape);

        if (roundScore == RoundScore.TIE) {
            roundMessage += "The opponent also played: " + sendShape.getName().toLowerCase();
        } else if (roundScore == RoundScore.LOSE) {
            roundMessage += sendShape.getName() + " gets beated by " + receivedShape.getName().toLowerCase() + ", sorry you lost this one";
        } else { // WIN
            roundMessage += sendShape.getName() + " beats " + receivedShape.getName().toLowerCase() + ", well done!";
        }

        // make changes in UI

        this.shifumiActivity.logAndToast(roundMessage);
        this.shifumiActivity.setShapeButtonsEnabled(true); // after round is over both can play again
        this.shifumiActivity.setScoreInTextView(score);
        this.shifumiActivity.setLastSendShape(sendShape);
        this.shifumiActivity.setLastReceivedShape(receivedShape);
        this.shifumiActivity.setRoundInfoVisible(false);
    }


}
