package jl.patou.shifumi.socket.game;

import java.io.IOException;
import java.io.ObjectOutputStream;

import jl.patou.shifumi.game.Shape;

/**
 * Created by patrick on 1/22/18.
 */

public class SendShapeThread extends Thread {


    Shape shapeToSend;
    ObjectOutputStream objectOutputStream;


    public SendShapeThread(Shape shapeToSend, ObjectOutputStream outputStream) {
        this.shapeToSend = shapeToSend;
        this.objectOutputStream = outputStream;
    }

    public void run() {
        try {
            objectOutputStream.writeObject(shapeToSend);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
