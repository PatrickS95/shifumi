package jl.patou.shifumi;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import jl.patou.shifumi.adapters.W2pDevicesAdapter;
import jl.patou.shifumi.fragments.ShifumiGameFragment;
import jl.patou.shifumi.fragments.StartpageFragment;
import jl.patou.shifumi.game.Shape;
import jl.patou.shifumi.p2p.PeerListenerP2p;
import jl.patou.shifumi.p2p.WiFiDirectBroadcastReceiver;
import jl.patou.shifumi.p2p.actionlisteners.DiscoverPeersActionListener;
import jl.patou.shifumi.socket.ShifumiClient;
import jl.patou.shifumi.socket.ShifumiDevice;
import jl.patou.shifumi.socket.ShifumiServer;
import jl.patou.shifumi.socket.SocketConnectionRole;

public class ShifumiActivity extends AppCompatActivity {

    public static final String TAG = "////ShifumiActivity////";
    private final IntentFilter intentFilter = new IntentFilter();
    // WIFI-DIRECT / P2P attributes
    private WifiP2pManager managerP2p; // contains (asynchronous) API for managing Wi-Fi peer-to-peer connectivity
    private Channel channelP2p;
    private BroadcastReceiver broadcastReceiver;
    private PeerListenerP2p peerListenerP2p;

    // attributes in addition to p2p attributes
    private boolean connectionIsActive; // may or may not helped, saw in Android Developer tutorial
    private List<WifiP2pDevice> peers;

    // Socket-connection attributes
    private ShifumiDevice shifumiServer;
    private ShifumiDevice shifumiClient;
    private InetAddress serverInetAddress;
    private SocketConnectionRole socketRole;

    // UI specific attributes
    private TextView role;
    private TextView ipText;
    private ListView w2pDevicesListView;

    // fragments
    private FragmentManager fragmentManager;
    private ShifumiGameFragment shifumiGameFragment;
    private StartpageFragment startpageFragment;

    private W2pDevicesAdapter w2pDevicesAdapter;

    // HANDLER FOR MULTITHREADING
    private Handler mUiHandler = new Handler(); // usage: mUiHandler.post // can be used directly after initialization


    // activity related methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shifumi);


        // P2P specific instructions
        this.connectionIsActive = false; // p2p is not enabled yet
        this.peers = new ArrayList<WifiP2pDevice>();
        initializeP2p();


        // UI initalizations
        this.ipText = (TextView) findViewById(R.id.ipAdressTextView);
        this.role = (TextView) findViewById(R.id.roleTexView);
        this.fragmentManager = getSupportFragmentManager();

        this.shifumiGameFragment = new ShifumiGameFragment();
        this.startpageFragment = new StartpageFragment();

        loadStartPageFragment();

    }

    @Override
    protected void onResume() {
        super.onResume();

        // onResume is also called when app starts but also when coming back
        // broadcastReceiver is initialized here to not be done twice
        initalizeBroadcaster();

    }


    @Override
    protected void onPause() {
        super.onPause();

        // is triggered when leaving the app: home or return button
        unregisterReceiver(broadcastReceiver);
    }

    public void setShapeButtonsEnabled(boolean enabled) {
        this.shifumiGameFragment.setShapeButtonsEnabled(enabled);
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deactivateConnection(false);

                        setResult(0);
                        finish();
                        finishAffinity();
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    // fragments related methods

    public void loadGameFragment() {

        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, this.shifumiGameFragment);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    public void loadStartPageFragment() {

        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, this.startpageFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    // p2p related methods

    public void initializeP2p() {

        // intialize IntentFilter
        this.intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION); // changes in the Wi-Fi P2P status.
        this.intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION); // changes in list of available peers.
        this.intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION); // Indicates the state of Wi-Fi P2P connectivity has changed.
        this.intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION); // indicates device's details changes.


        this.managerP2p = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        this.channelP2p = this.managerP2p.initialize(this, getMainLooper(), null); // we'll use this channel to connect to WIFI P2P
        //broadcastReceiver = new WiFiDirectBroadcastReceiver(managerP2p, channelP2p, this); // Listen for changes in P2P states

        this.managerP2p.discoverPeers(this.channelP2p, new DiscoverPeersActionListener()); // initiates peer discovery and returns
        //managerP2p.createGroup(channelP2p, new CreateGroupActionListener(this)); // currently not used. Android handles from itself

        this.connectionIsActive = false;
        this.peerListenerP2p = new PeerListenerP2p(this);
    }

    public void initalizeBroadcaster() {
        broadcastReceiver = new WiFiDirectBroadcastReceiver(managerP2p, channelP2p, this); // Listen for changes in P2P states
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * Methods deactivate Socket and W2p connection
     * Called when someone is leaving or the device itself wants to leave
     */
    public void deactivateConnection(boolean showToast) {


        // only do this if W2p is active
        if (!this.connectionIsActive)
            return;

        // deactivate socketConnection
        stopSocketConnection();
        stopSocketConnection();

        this.connectionIsActive = false;

        // deactive elements in View
        this.socketRole = null;
        this.role.setText(this.getResources().getString(R.string.role));
        this.ipText.setText(this.getResources().getString(R.string.ownerAdress));

        if (showToast)
            logAndToast(this.getResources().getString(R.string.otherPlayerLeft));


        //deactivate p2p
        managerP2p.removeGroup(channelP2p, null);


        // restart p2p
        initializeP2p();
        initalizeBroadcaster();
        loadStartPageFragment();

    }


    // socket related method

    public void startSocketConnection() {

        if (this.socketRole == SocketConnectionRole.CLIENT) {
            // start clientSocket

            if (shifumiClient == null) {
                Log.i(ShifumiActivity.TAG, "Start Client");
                this.shifumiClient = new ShifumiClient(this.serverInetAddress, ShifumiServer.SERVER_PORT, this);
                this.shifumiClient.start();
            }
        } else if (this.socketRole == SocketConnectionRole.SERVER) {

            if (shifumiServer == null) {
                Log.i(ShifumiActivity.TAG, "Start Server");
                this.shifumiServer = new ShifumiServer(ShifumiServer.SERVER_PORT, this);
                this.shifumiServer.start();
            }
        }

    }

    public void stopSocketConnection() {
        if (this.socketRole == SocketConnectionRole.CLIENT) {
            this.shifumiClient.stopSocketConnection();
        } else if (this.socketRole == SocketConnectionRole.SERVER) {
            this.shifumiServer.stopSocketConnection();
        }
    }


    /**
     * Method that charges shifumiClient or server to send the shape
     *
     * @param shape
     */
    public void sendShape(Shape shape) {
        if (socketRole == SocketConnectionRole.CLIENT)
            shifumiClient.sendShape(shape);

        else
            shifumiServer.sendShape(shape);
    }


    /**
     * Replaces list of peers and automatically updates UI
     *
     * @param wifiP2pDevices
     */
    public void updatePeerList(List<WifiP2pDevice> wifiP2pDevices) {

        this.peers.clear();
        this.peers.addAll(wifiP2pDevices);
        this.w2pDevicesAdapter.notifyDataSetChanged();
    }


    /**
     * Clears list of peers. Automatically updates UI
     */
    public void clearPeers() {
        this.peers.clear();
        this.w2pDevicesAdapter.notifyDataSetChanged();
    }


    public void activateP2pGroupInView(SocketConnectionRole role, InetAddress serverInetAddress) {
        this.role.setText(getString(R.string.role) + " " + role.name());
        this.ipText.setText(getString(R.string.ownerAdress) + " " + serverInetAddress.toString());
    }

    public void logAndToast(String string) {
        Log.i(TAG, string);
        Toast.makeText(this, string,
                Toast.LENGTH_LONG).show();
    }


    //////////////////// Setter ////////////////////////


    public void setRoundInfoVisible(Boolean visible) {
        this.shifumiGameFragment.setRoundInfoVisible(visible);
    }

    public void setTextRoundInfo(String roundInfo) {
        this.shifumiGameFragment.setRoundInfoVisible(true);
        this.shifumiGameFragment.setTextRoundInfo(roundInfo);
    }

    public void setLastSendShape(Shape sendShape) {
        this.shifumiGameFragment.setLastSendShape(sendShape);
    }

    public void setLastReceivedShape(Shape receivedShape) {
        this.shifumiGameFragment.setLastReceivedShape(receivedShape);
    }

    public void setW2pDevicesAdapter(W2pDevicesAdapter w2pDevicesAdapter) {
        this.w2pDevicesAdapter = w2pDevicesAdapter;
    }

    public void setScoreInTextView(int score) {
        this.shifumiGameFragment.setScoreInTextView(score);
    }

    public void setW2pDevicesListView(ListView w2pDevicesListView) {
        this.w2pDevicesListView = w2pDevicesListView;
    }

    public void setServerInetAddress(InetAddress serverInetAddress) {
        this.serverInetAddress = serverInetAddress;
    }

    public void setSocketRole(SocketConnectionRole socketRole) {
        this.socketRole = socketRole;
    }

    public List<WifiP2pDevice> getPeers() {
        return peers;
    }


    //////////////////// Getter ////////////////////////


    public WifiP2pManager getManagerP2p() {
        return managerP2p;
    }

    public Channel getChannelP2p() {
        return channelP2p;
    }

    public boolean isConnectionIsActive() {
        return connectionIsActive;
    }

    public void setConnectionIsActive(boolean connectionIsActive) {
        this.connectionIsActive = connectionIsActive;
    }

    public PeerListenerP2p getPeerListenerP2p() {
        return peerListenerP2p;
    }

    public Handler getmUiHandler() {
        return mUiHandler;
    }


}
